# Description

This module provide task definition to work with ECS.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 2.0 |
| <a name="requirement_null"></a> [null](#requirement\_null) | >= 2.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 2.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_container"></a> [container](#module\_container) | cloudposse/ecs-container-definition/aws | 0.57.0 |

## Resources

| Name | Type |
|------|------|
| [aws_ecs_task_definition.task](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_task_definition) | resource |
| [aws_iam_policy.policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.ecsTaskExecutionRole](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.ecsTaskExecutionRole_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.log](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_policy_document.assume_role_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_application"></a> [application](#input\_application) | n/a | <pre>object({<br>    name = string,<br>    container_name = string,<br><br>    host_port = string,<br>    container_port = string,    <br>    <br>    container_cpu = number<br>    container_memory = number<br>    reserved_cpu = number<br>    reserved_memory = number<br><br>    container_command = list(string)<br>  })</pre> | n/a | yes |
| <a name="input_image_url"></a> [image\_url](#input\_image\_url) | A map of application name to the image url. This information is provided by module ECR. | `map(string)` | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | AWS region | `string` | n/a | yes |
| <a name="input_taskexecutionrole"></a> [taskexecutionrole](#input\_taskexecutionrole) | Name of the task execution role to associate with the task\_definition | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_container_json"></a> [container\_json](#output\_container\_json) | Container definition in JSON format |
| <a name="output_task_definition_arn"></a> [task\_definition\_arn](#output\_task\_definition\_arn) | Backend definition ARN |


## Usage

```hcl-terraform
module "task_definition_backend" {
    source = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/task_definitions_for_ecs"
    application  = "<A object represents your application. See Input section for details>"
    image_url = "<A map provided by ECR resource after you have pushed docker images to ECR>"
    taskexecutionrole  = "Name of the task execution role which will be created for you"
}
```
